# -*- coding: utf-8 -*-

from django.http import JsonResponse
from django.shortcuts import render

from wm.utils import get_departure_time, get_bus_stop_list, get_bus_stop_direction


def home_page(request):
    return render(request, 'base.html', )


def response_json_bus_stop_name(request, pattern):
    """Response JSON file with an bus stop list, which are matching given pattern"""
    bus_stops_list = get_bus_stop_list(pattern)
    return JsonResponse(bus_stops_list)


def response_json_direction_list(request, name):
    """Response JSON file with travel directions, that are connected with particular bus stop name"""
    direction_list = get_bus_stop_direction(name)
    return JsonResponse(direction_list)


def response_json_time_list(request, symbol):
    """Response JSON file with an schedule of particular bus stop"""
    departure_time_list = get_departure_time(symbol)
    return JsonResponse(departure_time_list)
