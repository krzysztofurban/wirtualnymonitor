# -*- coding: utf-8 -*-
import time

import requests


def send_request(data):
    headers = {'Accept': 'text/javascript, text/html, application/xml, text/xml, */*',
               'Accept-Encoding': 'gzip, deflate',
               'Accept-Language': 'pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4',
               'Connection': 'keep-alive',
               'Content-Length': '271',
               'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
               'Cookie': 'Cookie:JSESSIONID=Rk8BgLd4ME5YoQgvkKYT2Zwh.undefined; cb-enabled=enabled;'
                         ' __utma=200167215.490795059.1489738664.1493357162.1496748117.8;'
                         ' __utmz=200167215.1489738664.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)',
               'Host': 'www.peka.poznan.pl',
               'Origin': 'https://www.peka.poznan.pl',
               'Referer': 'https://www.peka.poznan.pl/vm/',
               'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
                             'AppleWebKit/537.36 (KHTML, like Gecko) '
                             'Chrome/58.0.3029.110 Safari/537.36',
               'X-Prototype-Version': "1.7",
               'X-Requested-With': 'XMLHttpRequest',
               }
    url = 'https://www.peka.poznan.pl/vm/method.vm'
    params = {'ts': str(time.time()).replace(".", "")[:13]}
    response = requests.post(url, params=params, data=data, headers=headers)
    return response


def get_bus_stop_list(pattern):
    """Return prompt with bus stop list which matching pattern"""
    request_data = 'method=getStopPoints&p0={"pattern":"%s"}' % pattern
    request = send_request(request_data.encode(encoding='utf-8'))
    response_data = request.json()
    return response_data


def get_bus_stop_direction(name):
    """Takes full name of bus stop and return possibilities of direction that are connected with name"""
    request_data = 'method=getBollardsByStopPoint&p0={"name":"%s"}' % name
    request = send_request(request_data.encode(encoding='utf-8'))
    response_data = request.json()
    return response_data


def get_departure_time(symbol):
    """Takes bus stop symbol and return schedule"""
    request_data = 'method=getTimes&p0={"symbol":"%s"}' % symbol
    request = send_request(request_data.encode(encoding='utf-8'))
    response_data = request.json()
    return response_data
