from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home_page, name='home_page'),
    url(r'^stop_name/(?P<pattern>[-\w+% ./)(]+)$', views.response_json_bus_stop_name, name='bus_stop_list'),
    url(r'^direction_list/(?P<name>[-\w+% ./)(]+)$', views.response_json_direction_list, name='direction_list'),
    url(r'^time_list/(?P<symbol>[-\w+% ./)(]+)$', views.response_json_time_list, name='time_list'),
]
