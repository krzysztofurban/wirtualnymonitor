$(document).ready(function () {

    $('html').niceScroll({scrollspeed: 80});

    $('#search_bar').focus();


    $('.header').on('keyup', '#search_bar', function (event) {
        event.preventDefault();
        var pattern = $('#search_bar').val();
        var c = String.fromCharCode(event.keyCode);
        var isWordCharacter = c.match(/\w/);
        var isBackspaceOrDelete = (event.keyCode === 8 || event.keyCode === 46);
        if ((isWordCharacter || isBackspaceOrDelete) && pattern.length > 0) {
            if (pattern === '') {
                $('.content').html('');
            } else {
                var url = "stop_name/" + pattern;
                $.getJSON(url, function (data) {
                    data = data.success;
                    var new_content = "";
                    for (i = 0; i < data.length; i++) {
                        new_content += '<a href="" id="' + data[i].name + '" class="get_directions">';
                        new_content += '<div class="bus-stop-item"><h3>' + data[i].name + '</h3></div>';
                        new_content += '</a>';
                    }
                    $(".content").hide().html(new_content).slideDown();
                });
            }
        }
    });
    $('.header').on('click', '#search-button', function (event) {
        event.preventDefault();
        var pattern = $('#search_bar').val();
        if (pattern === '') {
            $('.content').html('');
        } else {
            var url = "stop_name/" + pattern;
            $.getJSON(url, function (data) {
                data = data.success;
                var new_content = "";
                for (i = 0; i < data.length; i++) {
                    new_content += '<a href="" id="' + data[i].name + '" class="get_directions">';
                    new_content += '<div class="bus-stop-item"><h3>' + data[i].name + '</h3></div>';
                    new_content += '</a>';
                }
                $(".content").hide().html(new_content).slideDown();
            });
        }

    });

    $('.content').on('click', '.get_directions', function (event) {
        event.preventDefault();
        var symbol = this.id;
        var url = "direction_list/" + symbol;
        $.getJSON(url, function (data) {
            var data = data.success.bollards;
            var new_content = "<h2>Wybierz kierunek:</h2>";
            for (i = 0; i < data.length; i++) {
                new_content += '<a href="" id="' + data[i].bollard.tag + '" class="get-time">';
                new_content += '<div class="bus-stop-direction-item"><h3>' + data[i].bollard.name + '</h3>';
                new_content += '<div class="bollard-direction"><p>';
                for (j = 0; j < data[i].directions.length; j++) {
                    new_content += '<span class="line-name">' + data[i].directions[j].lineName + '</span> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> ' + data[i].directions[j].direction + ', ';
                }
                new_content += '</p></div></div></a><div class="drop-down' + data[i].bollard.tag + '"></div>';
            }
            $(".content").html(new_content);
        });
    });


    var active_schedule = '';
    var hidden_bollard = '';
    var direction_header = '';

    $('.content').on('click', '.get-time', function (event) {
        event.preventDefault();
        var symbol = this.id;
        var url = "time_list/" + symbol;
        var container = $(active_schedule);
        container.stop( true, true );
        if (!container.is(event.target) && container.has(event.target).length === 0) {
            $(hidden_bollard).slideDown(600);
            container.slideUp(600).addClass('drop-down-style');
            $.getJSON(url, function (data) {
                bollard_data = data.success.bollard;
                data = data.success.times;
                // var new_content = '<p><i class="fa fa-refresh" aria-hidden="true"></i></p>';
                // var new_content = '<p><i class="fa fa-rocket" aria-hidden="true"></i> - Położenie potwierdzone przez GPS. Pojazd jest w trasie.</p>';
                var new_content = '<div class="schedule"><div class="schedule-header"><div class="departure-detail-line"><p>Linia</p></div>';
                new_content += '<div class="departure-detail"><p>Odjazd</p></div><div class="departure-detail"><p>Pozostało</p></div>';
                new_content += '<div class="departure-detail"><p>Kierunek</p></div><div class="departure-detail"><p>GPS</p></div></div>';
                for (i = 0; i < data.length; i++) {
                    new_content += '<div class="departure-row">';
                    new_content += '<div class="departure-detail-line"><p>' + data[i].line + '</p></div>';
                    new_content += '<div class="departure-detail"><p>' + data[i].departure.match(/\d{2}:\d{2}/) + '</p></div>';
                    new_content += '<div class="departure-detail">';
                    if (data[i].onStopPoint === false) {
                        new_content += '<p>' + data[i].minutes + ' minut</p>';
                    } else {
                        new_content += '<p><i class="fa fa-bus" aria-hidden="true"></i></p>';
                    }
                    new_content += '</div>';
                    new_content += '<div class="departure-detail"><p>' + data[i].direction + '</p></div>';
                    new_content += '<div class="departure-detail">';
                    if (data[i].realTime === true) {
                        new_content += '<p><i class="fa fa-rocket" aria-hidden="true"></i></p>';
                    } else {
                        new_content += '<p>Brak</p>';
                    }
                    new_content += '</div>';
                    new_content += '</div>';
                }
                active_schedule = ".drop-down" + symbol;
                hidden_bollard = '#' + symbol + ' .bollard-direction';
                $(hidden_bollard).stop( true, true ).slideUp(600);
                $(".drop-down" + symbol).html(new_content).slideDown(600).addClass('drop-down-style');
            });
        }
    });
});


